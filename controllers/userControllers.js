const User = require ('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')

//check if the email already exist

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then((result) =>{
		if (result.length > 0) {
			return true;
		}else{
			return false;
		}
		
	})
};

//registration
/*
Steps:
1.
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//save
	return newUser.save().then((user, error) => {
		//error
		if(error){
			return false;
		}else {
			return true;
		}
	})
};

//userAuth
/*
steps:
1. check the db if the user email exists
2. compare the password provided in the login form with the passowrd stored in the database
3. generate/ return a json web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		//if user does not exist
		if (result == null){
			return "not registered";
		}else {
			//user exist
			//create a variable "isPasswordCorrect" to return the result of the comparing the login form password and the database password
			//"compareSync" method that we used to compare a non encrypted password from the login form to the encrypted password retrieved from the db and returns "true" or "false" value depending on the result
			//(reqbody.password, result.password) nag ocompare si param1 sa param 2
			//A good practice for boolean variable/ constants is to use the word "is" or "are" at the beginnin in the form of is+Noun
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect){
				//generate access token
				//use the createAccessToken method defined in "auth.js" file
				//returning an object back to the frontend
				return {	accessToken: auth.createAccessToken(result.toObject())	}
			}else{
				//password did not match
				return false;
			}
		}
	})
};

// details by id
module.exports.postDetailsbyId = (reqBody) => {
	return User.findById({_id: reqBody._id}).then(result =>{
		if (result == null) {
			return false;
		}

		else{
			result.password ="";
			return result;
		}
		
	})
}

//get
const password ="";
module.exports.getDetailsbyId = (reqBody) => {
	return User.findById(reqBody).then((user,err) => {
		if (err){
			console.log(err);
			return false;
		} else{
			user.password = ""
			return user
			
		}
	})
}
const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')



//Routes for email checking in the DB
router.post('/checkEmail', (req,res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
})

router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})

//routes for authentication
router.post('/login', (req, res) =>{
	userController.loginUser(req.body).then(result => res.send(result));
})

//route for postget details

router.post('/details', (req,res) => {
	userController.postDetailsbyId(req.body).then(result => res.send(result));
})

//get for details via id

router.get('/getdetails', (req,res) =>{
	userController.getDetailsbyId(req.body).then(result => res.send(result))
})


module.exports = router;
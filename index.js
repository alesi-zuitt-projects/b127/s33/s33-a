const express = require('express');
const mongoose = require('mongoose');
/* allows our backend to be available to our front end application
allows us to control app's cross origin resource sharing settings*/
const cors = require('cors');

//allow access to the routes

const userRoutes = require('./routes/userRoutes');


const app = express();


//connect to our mongodb
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.nyots.mongodb.net/batch127_Booking?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true

		}
);

// prompts message at the terminal
let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("we're connected to the mongodb atlas"));

//app use
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//"/users" to be included for all the user routes
app.use('/users', userRoutes);


//Will used the defined port number for the application whenever an environment variable is available || will used
//4000 if non is defined
app.listen(process.env.PORT || 4000,() => {
	console.log(`API is now up @ port ${process.env.PORT || 4000}`)
})
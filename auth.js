const jwt = require('jsonwebtoken');
//User defined string data that will be used to create our JSON web token
//used in the algorithm for encrypting our data which makes it difficult to decode the informatin without the defined secret keyword
const secret = 'CourseBookngAPI';
//JSON web token or JWT is a way of securely passing information from the server to the front end or to other part of server
//information is kept secure through the use of the secret code
//secret is equal to a lock code.
//only the system knows the secret code that is encrypted

module.exports.createAccessToken = (user) => {
	//The data will be received from the registration form
	// when the user logs in, a token will be created with user's information

	const data ={
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin

	};

	//generate a json web token using the jwt's sign method (signature)
	//generates the token using the form data and the secret code with no addition options provided
	return jwt.sign(data, secret, {})
}